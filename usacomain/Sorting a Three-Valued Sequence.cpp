/*
ID: n.nekog1
LANG: C++
TASK: sort3
*/
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

int count(vector<int>& a, vector<int>& b) {
    int count = 0;
    for (int i = 0; i < a.size(); i++)
        if(a[i] != b[i])
            count ++;
    return count;
}

int main () {
    ifstream in ("sort3.in");
    ofstream out ("sort3.out");
    int n;
    vector<int> nums(3, 0);
    in >> n;
    vector<int> a(n);
    vector<int> b;
    for (int i = 0; i < n; i++) {
        in >> a[i];
        nums[a[i] - 1]++;
    }
    for (int i = 1; i <= 3; i++)
        for(int j = 0; j < nums[i - 1]; j++)
            b.push_back(i);
    cout << nums[0] << " " << nums[1] << " " << nums[2] << "\n";
    // cout << count(a, b) << " \n";
    out << (count(a, b) + 1) / 2 << "\n";

    return 0;
}