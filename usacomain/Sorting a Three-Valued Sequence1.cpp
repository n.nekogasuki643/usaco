/*
ID: n.nekog1
LANG: C++
TASK: sort3
*/
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

int main () {
    ifstream in("sort3.in");
    ofstream out("sort3.out");
    int n;
    in >> n;
    vector<int> nums(3, 0);
    vector<int> a(n);
    vector<int> b;
    vector<pair<int, int> > include(3);

    int x[] = {2, 1, 1};
    int y[] = {3, 3, 2};

    for (int i = 0; i < n; i++) {
        in >> a[i];
        nums[a[i] - 1]++;
    }
    for (int i = 0; i < 3; i++) {
        int m = 0, n = 0;
        for (int j = 0; j < nums[i]; j++) {
            // b.push_back(i + 1);
            if(a[j] == x[i])
                m++;
            if(a[j] == y[i])
                n++;
            include[i] = make_pair(m , n);
        }
    }
     
    


    return 0;
}