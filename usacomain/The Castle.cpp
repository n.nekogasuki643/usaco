/*
ID: n.nekog1
LANG: C++
TASK: castle
*/
#include <iostream>
#include <fstream>
#include <math.h>
#include <vector>

using namespace std;
vector<vector<bool> > graph;
int m, n, cur;
pair< vector<vector<bool> >, vector<vector<bool> > >  draw (vector<vector<int> > a) {
    vector<vector<bool> > castle1(n + 1, vector<bool> (m, 1));
    vector<vector<bool> > castle2(n, vector<bool> (m + 1, 1));
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < m; j++) {
            int wall = 8;
            for(int k = 0; k < 4; k++) {
                if(wall > a[i][j]) {
                    if(k == 0)
                        castle1[i + 1][j] = 0;
                    if(k == 2) 
                        castle1[i][j] = 0;
                    if(k == 1) 
                        castle2[i][j + 1] = 0;
                    if(k == 3)
                        castle2[i][j] = 0;
                } else 
                    a[i][j] -= wall;
                wall /= 2;
            }
        }   
    }
    return make_pair(castle1, castle2);
}
void connection (int i, int j, vector<vector<bool> > castle1, vector<vector<bool> > castle2) {
    for(int i = 0; i < 2; i++) {
        
    } 
}

int main() {
    ifstream in ("castle.in");
    ofstream out ("castle.out");

    in >> m >> n;
    // a.resize(n); 
    vector<vector<bool> > castle1(n + 1, vector<bool> (m));
    vector<vector<bool> > castle2(n, vector<bool> (m + 1));
    vector<vector<int> > a (n, vector<int> (m));
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < m; j++) 
            in >> a[i][j] ;
    }
    pair< vector<vector<bool> >, vector<vector<bool> > > tmp = draw(a);
    castle1 = tmp.first;
    castle2 = tmp.second;
    // for(int i = 1; i < n + 1; i++)
    //     castle1[i - 1]  = castle1[i];
    // castle1.pop_back();
    // castle1.pop_back();
    // for(int i = 0; i < n; i++) {
    //     for(int j = 1; j <= m; j++) {
    //         castle2[i][j - 1] = castle2[i][j];
    //     }
    //     castle2[i].pop_back();
    //     castle2[i].pop_back();
    // }
    
    
    
    for(int i = 0; i < castle2.size(); i++) {
        for(int j = 0; j < castle1[i].size(); j++) {
            cout << "  " << castle1[i][j] << " "; 
        }
        cout << endl;
        for(int j = 0; j < castle2[i].size(); j++) {
            cout << castle2[i][j] << "   "; 
        }
        cout << endl;
    }
    for(int j = 0; j < castle1[castle1.size() - 1].size(); j++) {
        cout << "  " << castle1[castle1.size() - 1][j] << " "; 
    }
    cout << endl;

    return 0;

}