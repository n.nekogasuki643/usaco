/*
ID: n.nekog1
LANG: C++
TASK: dualpal
*/
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;
bool Palcheck(string a, string b) {
    return a == b;
}
bool IsPaltoBase(int n, int number) {
    string rever = "", result = "";
    while(number != 0){
        result += char(number % n + '0');
        number /= n;
    }
    string tmp = result;
    int length = result.length();
    for(int i = 1; i <= length; i++)
        rever += result[length - i];
    cout << rever << " " << result << " hmmm\n";
    return Palcheck(rever, result);
}
int main () {
    ifstream in ("dualpal.in");
    ofstream out ("dualpal.out");
    int n, s, count = 0, i = 1, base = 2, countbase = 0;
    in >> n >> s;
    while(count < n) {
        s++;
        while(base <= 10 && countbase < 2) {
            if(IsPaltoBase(base, s))
                countbase++;
            base ++;
        }
        if(countbase == 2) {
            out << s << "\n";
            count ++;
        }
        base = 2;
        countbase = 0;
    }

    return 0;
}