/*
ID: n.nekog1
LANG: C++
TASK: pprime
*/
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;
vector<int> results;
bool checkPal (int b) {
    int temp = 0, a = b;
    while (a != 0) {
        temp = temp * 10 + (a % 10);
        a /= 10;
    }
    return b == temp;
}
void Sieve(int a, int b) { 
    vector<bool> prime (b + 1, 1);
  
    for (int p = 2; p * p <= b; p++) { 
        if (prime[p] == true) { 
            for (int i = p * p; i <= b; i += p) 
                prime[i] = false; 
        } 
    } 
  
    for (int p = a; p <= b; p++) 
        if (prime[p])
            if(checkPal(p))
                results.push_back(p);
} 
int main() {
    ifstream in ("pprime.in");
    ofstream out ("pprime.out");
    int a, b;
    in >> a >> b;
    Sieve(a, b);
    for(int i = 0; i < results.size(); i++)
        out << results[i] << "\n";


    return 0;
}